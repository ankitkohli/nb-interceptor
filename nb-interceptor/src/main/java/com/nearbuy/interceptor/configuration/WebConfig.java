package com.nearbuy.interceptor.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import com.nearbuy.interceptor.handler.RequestInterceptor;

@Configuration
public class WebConfig extends WebMvcConfigurationSupport{

	@Autowired
	RequestInterceptor requestInterceptor;
	
	@Override
	   public void addInterceptors(InterceptorRegistry registry) {

		// Register interceptor with multiple path patterns
	      registry.addInterceptor(requestInterceptor)
	              .addPathPatterns(new String[] { "/request" });
	   }

}
